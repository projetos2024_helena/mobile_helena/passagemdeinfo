import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Ir ao Mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "Super SS",
    },
    {
      id: 1,
      title: "Ir ao Colegio",
      date: "2024-02-27",
      time: "09:00",
      address: "Escola",
    },
    {
      id: 1,
      title: "Ir ao Trabalho",
      date: "2024-02-27",
      time: "08:00",
      address: "Trabalho",
    },
  ];
  const taskPress = (task) => {
    navigation.navigate("DetalhesDasTarefas", {task});
  };

  return (
    <View>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default TaskList;
